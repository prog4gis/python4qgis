---
title: 'Raster, chargement et manipulation'
author: "Victor PÉNOT"
weight : 4 
pre: "<b>TP 4. </b>"
output:
  bookdown::html_document2:
    #css: style.css
    toc: yes
    toc_float:
      collapsed: yes
      smooth_scroll: yes
    theme: cerulean #united
    highlight: tango
  bookdown::pdf_book:
    toc: yes
    #includes: 
      #in_header: preambule.tex
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      eval=FALSE,
                      message=FALSE, 
                      warning=FALSE)

```

# Definition initiale des chemins vers les jeux de données
```{python}
import os 
# Repertoire de travail
work_path="/home/victor/Bureau/LPTIC_Carcassone/COURS/TP4/RES"
# Definition du repertoire de travail
os.chdir(work_path)
# Repertoire des donnees raster
path_to_raster="../../DATA/SENTINEL"
# Repertoire des donnees vecteur 
path_to_vector="../../DATA/ZONE_ETUDE"
```

# Utilisation du module ```glob``` 
Nous devons ouvrir 4 couches raster. Les noms des couches Sentinel 2 fournies par Theia sont assez complexes et fournissent un nombre important d'informations sur la couches.

## Obtention de la liste des dates de debut de prise de vue
Plutôt que de parser manuellement les fichiers afin de connaître les dates de prises de vue, listons-les simplement.
Pour cela obtenons la liste des fichiers Sentinel au format ```tif``` via le module [```glob```](https://docs.python.org/fr/3.6/library/glob.html) qui fournit la méthode ```glob```. Cette dernière renvoie la liste des fichiers contenant (ou non) certains motifs. Ici nous utiliserons la [widlcart ```*```](https://pymotw.com/2/glob/). Les motifs recherchés seront ici "SENTINEL2" au début du nom de fichiers  et ".tif" en fin de nom, tous caractère étant autorisé entre les deux (utilisation de ```*```).

```{python}
# Chargement des modules glob et os
import glob
# Obtention des chemins relatifs des rasters sentinel du dossier
raster_path=glob.glob(os.path.join(path_to_raster,"SENTINEL2*.tif"))
print("Nombre de couches dans le dossier : ",len(raster_path))
```
Pour n'obtenir que le nom des fichiers et non pas les chemins relatifs recréons une nouvelles liste :
```{python}
raster_name=[ os.path.basename(path) for path in raster_path ]
print(raster_name)
```
 

Obtenons maintenant la liste des dates. En observant la syntaxe d'un nom de couche Sentinel, nous remarquons que la date se trouve en deuxième positon après le ```_``` et en première position avant le  ```-``` 
```{python}
# Creation d une liste vide 
date=[]
# Remplissage de la liste via une Iteration sur le nom des couches raster : un peu de POO 
[date.append(d.split("_")[1].split("-")[0]) for d  in raster_name if not d.split("_")[1].split("-")[0] in date  ]
# Affichage des dates des couches sentinel
print(date)
```

::: {.homework data-latex=""}
Créer une nouvelle liste ```band``` qui stockera les différentes bandes disponibles parmi les rasters. On supposera que chaque bande est disponible à chaque date.
:::

```{python}
# Creation d une liste vide pour le stockage des bandes
band=[]
# Remplissage de la liste via une iteration sur le nom des couches raster : un peu de POO 
[band.append(d.split("_")[7].split(".")[0]) for d  in raster_name if not d.split("_")[7].split(".")[0] in band]
# Affichage des bandes des couches sentinel
print(band)
```

::: {.homework data-latex="""}
Si vous avez le temps et l'envie, créer une nouvelle liste ```date_band``` qui stockera les différentes bandes disponibles à une date donnée parmi les rasters. Cela permet de vérifier que chaque bande est disponible à chaque date.
:::
<details>
  <summary>**SOLUTION**</summary>
```{python}
# Creation d une liste vide pour le stockage des bandes et des dates
date_band=[]
# Remplissage de la liste via une iteration sur le nom des couches raster : un peu de POO 
[date_band.append(d.split("_")[7].split(".")[0]+"_"+d.split("_")[1].split("-")[0]) for d  in raster_name if not d.split("_")[7].split(".")[0]+"_"+d.split("_")[1].split("-")[0] in date_band]
# Affichage des bandes et des dates des couches sentinel
print(date_band)
```
</details>

# Chargement d'une couche raster 

Nous allons commencer par charger une couche raster dans le projet  afin de visionner son étendue par rapport à l'étendue de la zone d'étude.
Nous commencerons par la couche ```B8A``` en date du ```20171007``` stockée sous le nom ```b8a_20171007```, qui sera aussi la variable.
Aussi simplement qu'une couche vecteur, cette couche raster s'importe dans la session Python puis dans le projet en instanciant la classe ```QgsRasterLayer``` avec la données d'un chemin vers la couche :
```{python}
# Chargement d un raster dans le projet par exemple la couche b8A à la date 20171007
# Le module glob nous evite de trop ecrire.
b8a_20171007=QgsRasterLayer(glob.glob(os.path.join(path_to_raster,"*20171007*B8A*"))[0])
# Ajout de la couche au projet
QgsProject.instance().addMapLayer(b8a_20171007)
```

::: {.homework data-latex=""}
Charger la couche ```ZONE_ETUDE.shp``` dans le projet.
:::

<details>
  <summary>**SOLUTION**</summary>
```{python}
# Chargement de la zone d etude
zone=QgsVectorLayer(os.path.join(path_to_vector,"ZONE_ETUDE.shp"),"zone_etude","ogr")
# et affichage dans le projet
QgsProject.instance().addMapLayer(zone)
```
</details>


Il va nous falloir restreindre la couche raster. Puisque nous n'avons plus besoin de cette dernière supprimons-la du projet et de la session Python (gain de mémoire)
```{python}
# Suppression de la couche raster du projet
QgsProject.instance().removeMapLayer(b8a_20171007.id())
del b8a_20171007
iface.mapCanvas().refresh()
```


# Decoupage d'une couche raster à partir d'une couche vectorielle

Il est évident que la couche raster est beaucoup trop étendue (de même que les autres). Afin de réduire les temps de calculs, nous allons découper toutes les couches rasters grâce au programme [```gdal:cliprasterbyextent```](https://docs.qgis.org/3.10/en/docs/user_manual/processing_algs/gdal/rasterextraction.html#clip-raster-by-mask-layer) de l'extension ```processing```.

::: {.homework data-latex=""}
Grâce à une boucle sur les fichiers raster et au programme de découpage, écrire un programme permettant de découper les rasters selon l'étendue de la couche vectorielle ```zone_etude``` . On enregistrera ces couches permanentes dans le répertoire de travail en ajoutant le suffixe ```CLIP``` au nom de la couche en entrée.
:::

<details>
  <summary>**SOLUTION**</summary>
```{python}
# Decoupage de toutes les couches presentes dans le dossier SENITINEL
# selon l emprise de la zone d etude 
for layer_path in raster_path :
    #chargement de la couche raster
    raster_layer=QgsRasterLayer(layer_path)
    # nom de la couche en sortie
    output="CLIP_"+ os.path.basename(layer_path)
    # dictionnaire des parametres 
    parameters={
    'INPUT':raster_layer,
    'MASK':zone,
    'OUTPUT':output}
    processing.run("gdal:cliprasterbymasklayer", parameters)

```

Chargeons maintenant les couches rasters dans 4 variables nommées selon la syntaxe ```b*num_couche*_*date*```
```{python}
b8A_20171007=QgsRasterLayer(glob.glob("*20171007*B8A*.tif")[0],"b8A_20171007")
QgsProject.instance().addMapLayer(b8A_20171007)

b8A_20171017=QgsRasterLayer(glob.glob("*20171017*B8A*.tif")[0],"b8A_20171017")
QgsProject.instance().addMapLayer(b8A_20171017)

b12_20171007=QgsRasterLayer(glob.glob("*20171007*B12*.tif")[0],"b12_20171007")
QgsProject.instance().addMapLayer(b12_20171007)

b12_20171017=QgsRasterLayer(glob.glob("*20171017*B12*.tif")[0],"b12_20171017")
QgsProject.instance().addMapLayer(b12_20171017)

# Puis zoom sur une couche 
iface.mapCanvas().setExtent(zone.extent())
```

# Calcul d'un indice : calculatrice raster
Nous allons désormais calculer l'indice $NBR$ à chaque date. Il nous faut effectuer le calcul suivant à partir des deux couches d'indices obtenus à une date  $$NBR=\dfrac{B8A-B12}{B8A+B12}$$
Nous allons de nouveau utiliser un programme de l'extension ```processing``` : [```gdal:rastercalculator```](https://docs.qgis.org/3.10/en/docs/user_manual/processing_algs/gdal/rastermiscellaneous.html#raster-calculator)

Commençons par calculer le $NBR$ en date du 20171007. Définissons le dictionnaire des paramètres, exécutons le programme et chargeons la couche 
```{python}
# Calcul du NBR le 20171007
output="NBR_20171007.tif"
parameters={
    # couche en entree b8a
    'INPUT_A':b8A_20171007,
    # numero de la bande
    'BAND_A':1,
    # couche en entree b12
    'INPUT_B':b12_20171007,
    # numero de la bande
    'BAND_B':1,
    # expression a evaluer
    'FORMULA':'(A-B)/(A+B)',
    'NO_DATA':-9999,
    # type de float en sortie
    'RTYPE':5,
    # couche en sortie
    'OUTPUT':output}
# execution du programme
output=processing.run('gdal:rastercalculator',parameters)['OUTPUT']
# chargement de la couche dans le projet
nbr_20171007=QgsProject.instance().addMapLayer(QgsRasterLayer(output,'nbr_20171007'))
```




::: {.homework data-latex=""}
Faire de même à la date 20171017.
:::

<details>
  <summary>**SOLUTION**</summary>
```{python}
# Calcul du NBR le 20201017
output="NBR_20171017.tif"
parameters={
    # couche en entree b8a
    'INPUT_A':b8A_20171017,
    # numero de la bande
    'BAND_A':1,
    # couche en entree b12
    'INPUT_B':b12_20171017,
    # numero de la bande
    'BAND_B':1,
    # expression a evaluer
    'FORMULA':'(A-B)/(A+B)',
    'NO_DATA':-9999,
    # type de float en sortie
    'RTYPE':5,
    # couche en sortie
    'OUTPUT':output}
# execution du programme
output=processing.run('gdal:rastercalculator',parameters)['OUTPUT']
# chargement de la couche dans le projet
nbr_20171017=QgsProject.instance().addMapLayer(QgsRasterLayer(output,'nbr_20171017'))
```
</details>

::: {.homework data-latex=""}
En vous appuyant sur l'exemple précédent, évaluer l'évolution du $NBR$ entre les deux dates, definie par 
$$\Delta NBR =NBR_{prefire}-NBR_{postfire}$$
On donnera comme nom de fichier :```DNBR_20171007_20171017.tif```
:::
<details>
  <summary>**SOLUTION**</summary>
```{python}
# Calcul du dNBR
output="DNBR_20171007_20171017.tif"
parameters={
    # couche en entree b8a
    'INPUT_A':nbr_20171007,
    # numero de la bande
    'BAND_A':1,
    # couche en entree b12
    'INPUT_B':nbr_20171017,
    # numero de la bande
    'BAND_B':1,
    # expression a evaluer
    'FORMULA':'(A-B)',
    'NO_DATA':-9999,
    # type de float en sortie
    'RTYPE':5,
    # couche en sortie
    'OUTPUT':output}
# execution du programme
output=processing.run('gdal:rastercalculator',parameters)['OUTPUT']
# chargement de la couche dans le projet
dnbr=QgsProject.instance().addMapLayer(QgsRasterLayer(output,'dnbr'))
```
</details>

# Seuillage de la couche 
Le calcul de la couche précédente du $\Delta NBR$ nous permet de définir le contour de l'incendie. Des études scientifiques ont montré que pour un $\Delta NBR$ supérieur à $0,10$ la zone pouvait être considérée comme incendiée.

::: {.homework data-latex=""}
En utilisant la calculatrice raster, effectuer un seuillage à $0,10$ grâce à l'expression ```A>0.10```. On enregistrera la couche sous le nom ```raster_fire.tif```
:::
<details>
  <summary>**SOLUTION**</summary>
```{python}
# Calcul et affichage du raster de la zone de feu
output="raster_fire.tif"
parameters={
    'INPUT_A':dnbr,
    'BAND_A':1,
    'FORMULA':'A>0.1',
    'NO_DATA':-9999,
    'RTYPE':5,
    'OUTPUT':output}
processing.run('gdal:rastercalculator',parameters)
raster_fire=QgsRasterLayer(output,os.path.basename(output)[:-4])
QgsProject.instance().addMapLayer(raster_fire)
```
</details>

# Conclusion
Comparer "visuellement" le raster précédemment obtenu  avec la couche de comparaison de référence de l'incendie disponible dans le dossier ```DATA/COMP```

```{r echo=FALSE, fig.cap = "Détection satellite : un peu de bruit", eval=TRUE, out.width='80%', fig.align = "center", fig.margin=FALSE}
knitr::include_graphics('images_qgis/1.png')
```

```{r echo=FALSE, fig.cap = "Couche officielle ... déterminée par satellite !", eval=TRUE, out.width='80%', fig.align = "center", fig.margin=FALSE}
knitr::include_graphics('images_qgis/2.png')
```










